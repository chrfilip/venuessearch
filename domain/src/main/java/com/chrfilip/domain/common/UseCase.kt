package com.chrfilip.domain.common

import com.chrfilip.domain.failure.Failure
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

abstract class UseCase<in Params, out Type> where Type : Any? {

    abstract suspend fun run(params: Params): Either<Failure, Type>

}