package com.chrfilip.domain.koin

import com.chrfilip.domain.usecase.VenueDetailsUseCase
import com.chrfilip.domain.usecase.VenuesSearchUseCase
import org.koin.dsl.module

val domainModule = module {
    single { VenuesSearchUseCase(get()) }
    single { VenueDetailsUseCase(get()) }
}
