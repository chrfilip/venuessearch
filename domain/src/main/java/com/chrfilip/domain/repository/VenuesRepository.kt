package com.chrfilip.domain.repository

import com.chrfilip.domain.common.Either
import com.chrfilip.domain.failure.Failure
import com.chrfilip.domain.model.SearchParams
import com.chrfilip.domain.model.VenueItem
import com.chrfilip.domain.model.VenueId

interface VenuesRepository {

    suspend fun searchVenues(searchParams: SearchParams): Either<Failure, List<VenueItem>>
    suspend fun venueDetails(venueId: VenueId): Either<Failure, VenueItem>

}