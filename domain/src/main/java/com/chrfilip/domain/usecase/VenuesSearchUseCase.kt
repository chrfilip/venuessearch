package com.chrfilip.domain.usecase

import com.chrfilip.domain.common.Either
import com.chrfilip.domain.common.UseCase
import com.chrfilip.domain.failure.Failure
import com.chrfilip.domain.model.SearchParams
import com.chrfilip.domain.model.VenueItem
import com.chrfilip.domain.repository.VenuesRepository

class VenuesSearchUseCase(private val venuesRepository: VenuesRepository) : UseCase<SearchParams, List<VenueItem>>() {
    override suspend fun run(params: SearchParams): Either<Failure, List<VenueItem>> {
        return venuesRepository.searchVenues(searchParams = params)
    }
}