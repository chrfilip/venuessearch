package com.chrfilip.domain.usecase

import com.chrfilip.domain.common.Either
import com.chrfilip.domain.common.UseCase
import com.chrfilip.domain.failure.Failure
import com.chrfilip.domain.model.SearchParams
import com.chrfilip.domain.model.VenueId
import com.chrfilip.domain.model.VenueItem
import com.chrfilip.domain.repository.VenuesRepository

class VenueDetailsUseCase(private val venuesRepository: VenuesRepository) : UseCase<VenueId, VenueItem>() {
    override suspend fun run(params: VenueId): Either<Failure, VenueItem> {
        return venuesRepository.venueDetails(venueId = params)
    }
}