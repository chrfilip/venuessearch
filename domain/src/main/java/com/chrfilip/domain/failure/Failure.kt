package com.chrfilip.domain.failure

/**
 * Collection of failures
 */
sealed class Failure(val message: String) {

    data class ApiFailure(val errorMessage: String, val errorCode: Int) : Failure(errorMessage)

    data class NetworkFailure(val errorMessage: String) : Failure(errorMessage)

    data class CacheFailure(val errorMessage: String) : Failure(errorMessage)

    object Undefined : Failure("Unknown error")
}

