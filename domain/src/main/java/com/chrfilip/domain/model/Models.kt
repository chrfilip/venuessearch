package com.chrfilip.domain.model

inline class VenueId(val value: String)
inline class PhotoUrl(val value: String)

data class ContactInfo(
    val phone: String? = null,
    val twitter: String? = null,
    val instagram: String? = null
)

class VenueItem(
    val id: VenueId,
    val name: String,
    val address: String,
    val description: String? = null,
    val photoUrl: PhotoUrl? = null,
    val contactInfo: ContactInfo? = null,
    val rating: Double? = null
)

data class SearchParams(
    val intent: String,
    val query: String,
    val near: String,
    val radius: Int,
    val limit: Int
)