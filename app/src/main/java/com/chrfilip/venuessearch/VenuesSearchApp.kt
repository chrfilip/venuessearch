package com.chrfilip.venuessearch

import android.app.Application
import com.chrfilip.data.koin.dataModule
import com.chrfilip.domain.koin.domainModule
import com.chrfilip.presentation.koin.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class VenuesSearchApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@VenuesSearchApp)
            modules(listOf(
                dataModule,
                domainModule, presentationModule))
        }
    }
}