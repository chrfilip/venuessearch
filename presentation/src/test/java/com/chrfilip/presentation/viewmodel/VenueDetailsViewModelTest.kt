package com.chrfilip.presentation.viewmodel

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.core.app.ApplicationProvider
import com.chrfilip.domain.common.Either
import com.chrfilip.domain.failure.Failure
import com.chrfilip.domain.koin.domainModule
import com.chrfilip.domain.model.VenueId
import com.chrfilip.domain.model.VenueItem
import com.chrfilip.domain.usecase.VenueDetailsUseCase
import com.chrfilip.presentation.koin.presentationModule
import com.nhaarman.mockitokotlin2.stub
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.*
import org.junit.runner.RunWith
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.test.AutoCloseKoinTest
import org.koin.test.get
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import kotlin.test.assertEquals


@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P])  // When targeting API 29 robolectric requires Java 9
class VenueDetailsViewModelTest : AutoCloseKoinTest() {

    @get:Rule
    var rule = InstantTaskExecutorRule()

    private val job: Job = SupervisorJob()
    private val testCoroutineDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testCoroutineDispatcher + job)

    @Mock
    lateinit var venueDetailsUseCase: VenueDetailsUseCase

    @Mock
    lateinit var stateObserver: Observer<DetailsState>

    lateinit var sut: VenueDetailsViewModel

    private val initialState = DetailsState(
        isProgressVisible = false,
        message = "",
        venue = null
    )

    private val venueId = VenueId("1")

    private val venueItem = VenueItem(venueId, "pizza", "somewhere")

    @Before
    fun setUp() {
        initMocks(this)

        startKoin {
            androidContext(ApplicationProvider.getApplicationContext())
            modules(listOf(presentationModule, domainModule))
        }

        sut = VenueDetailsViewModel(get(), venueDetailsUseCase, initialState)
        sut.uiState.observeForever(stateObserver)
    }

    @Test
    fun `when details call fails then the error message should be shown`() {
        testScope.runBlockingTest {
            val venueId = VenueId("")

            // given
            venueDetailsUseCase.stub {
                onBlocking { run(venueId) }.thenReturn(Either.Error(Failure.Undefined))
            }

            // when
            sut.dispatchAction(DetailsAction.LoadDetails(venueId))

            // then
            assertEquals("Unknown error", sut.uiState.value?.message)
            assertEquals(false, sut.uiState.value?.isProgressVisible)
            assertEquals(null, sut.uiState.value?.venue)
        }
    }

    @Test
    fun `when details call succeeds then progress not should be visible and message should be empty`() {
        testScope.runBlockingTest {
            // given
            venueDetailsUseCase.stub {
                onBlocking { run(venueId) }.thenReturn(
                    Either.Success(venueItem)
                )
            }

            // when
            sut.dispatchAction(DetailsAction.LoadDetails(venueId))

            // then
            assertEquals("", sut.uiState.value?.message)
            assertEquals(false, sut.uiState.value?.isProgressVisible)
        }
    }

    @Test
    fun `when details call succeeds then the venue name and address should be correct`() {
        testScope.runBlockingTest {

            // given
            venueDetailsUseCase.stub {
                onBlocking { run(venueId) }.thenReturn(
                    Either.Success(venueItem)
                )
            }

            // when
            sut.dispatchAction(DetailsAction.LoadDetails(venueId))

            // then
            assertEquals("pizza", sut.uiState.value?.venue?.name)
            assertEquals("somewhere", sut.uiState.value?.venue?.address)
        }
    }

    @Test
    fun `when details call succeeds and venue has partial data then empty data should be filled with dashes`() {
        testScope.runBlockingTest {

            // given
            venueDetailsUseCase.stub {
                onBlocking { run(venueId) }.thenReturn(
                    Either.Success(venueItem)
                )
            }

            // when
            sut.dispatchAction(DetailsAction.LoadDetails(venueId))

            // then
            assertEquals("--", sut.uiState.value?.venue?.contactInfo)
            assertEquals("--", sut.uiState.value?.venue?.rating)
            assertEquals("--", sut.uiState.value?.venue?.description)
        }
    }

    @After
    fun tearDown() {
        sut.uiState.removeObserver(stateObserver)
    }


}