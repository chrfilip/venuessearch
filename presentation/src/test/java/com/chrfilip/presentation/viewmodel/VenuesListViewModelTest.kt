package com.chrfilip.presentation.viewmodel

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.core.app.ApplicationProvider
import com.chrfilip.domain.common.Either
import com.chrfilip.domain.failure.Failure
import com.chrfilip.domain.koin.domainModule
import com.chrfilip.domain.model.VenueId
import com.chrfilip.domain.model.VenueItem
import com.chrfilip.domain.usecase.VenuesSearchUseCase
import com.chrfilip.presentation.koin.presentationModule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.stub
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.*
import org.junit.runner.RunWith
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.test.AutoCloseKoinTest
import org.koin.test.get
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import kotlin.test.assertEquals


@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P])  // When targeting API 29 robolectric requires Java 9
class VenuesListViewModelTest : AutoCloseKoinTest() {

    @get:Rule
    var rule = InstantTaskExecutorRule()

    private val job: Job = SupervisorJob()
    private val testCoroutineDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testCoroutineDispatcher + job)

    @Mock
    lateinit var venuesSearchUseCase: VenuesSearchUseCase

    @Mock
    lateinit var stateObserver: Observer<SearchState>

    lateinit var sut: VenuesListViewModel

    private val initialState = SearchState(
        isProgressVisible = false,
        message = "",
        venues = emptyList()
    )

    private val venue1 = VenueItem(VenueId("1"), "new york pizza", "somewhere 33")
    private val venue2 = VenueItem(VenueId("2"), "bob's burgers", "somewhere else 42")
    private val venue3 = VenueItem(VenueId("3"), "koi sushi bar", "route 3487")

    private val venueItems = listOf(venue1, venue2, venue3)

    @Before
    fun setUp() {
        initMocks(this)

        startKoin {
            androidContext(ApplicationProvider.getApplicationContext())
            modules(listOf(presentationModule, domainModule))
        }

        sut = VenuesListViewModel(get(), venuesSearchUseCase, initialState)
        sut.uiState.observeForever(stateObserver)
    }

    @Test
    fun `when search call fails then the error message should be shown`() {
        testScope.runBlockingTest {
            // given
            venuesSearchUseCase.stub {
                onBlocking { run(any()) }.thenReturn(Either.Error(Failure.Undefined))
            }

            // when
            sut.dispatchAction(SearchAction.SearchVenues("burger"))

            // then
            assertEquals("Unknown error", sut.uiState.value?.message)
            assertEquals(false, sut.uiState.value?.isProgressVisible)
            assertEquals(0, sut.uiState.value?.venues?.size)
        }
    }

    @Test
    fun `when search call succeeds then progress should not be visible and message should be empty`() {
        testScope.runBlockingTest {
            // given
            venuesSearchUseCase.stub {
                onBlocking { run(any()) }.thenReturn(
                    Either.Success(venueItems)
                )
            }

            // when
            sut.dispatchAction(SearchAction.SearchVenues("sushi"))

            // then
            assertEquals("", sut.uiState.value?.message)
            assertEquals(false, sut.uiState.value?.isProgressVisible)
        }
    }

    @Test
    fun `when search call succeeds then venue items list should have the correct size`() {
        testScope.runBlockingTest {
            val venueId = VenueId("1")

            val venueItem = VenueItem(venueId, "pizza", "somewhere")
            // given
            venuesSearchUseCase.stub {
                onBlocking { run(any()) }.thenReturn(
                    Either.Success(venueItems)
                )
            }

            // when
            sut.dispatchAction(SearchAction.SearchVenues("steak"))

            // then
            assertEquals(3, sut.uiState.value?.venues?.size)
        }
    }

    @After
    fun tearDown() {
        sut.uiState.removeObserver(stateObserver)
    }


}