package com.chrfilip.presentation.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chrfilip.domain.model.VenueId
import com.chrfilip.presentation.R
import com.chrfilip.presentation.common.observe
import com.chrfilip.presentation.common.setVisible
import com.chrfilip.presentation.viewmodel.DetailsAction
import com.chrfilip.presentation.viewmodel.DetailsState
import com.chrfilip.presentation.viewmodel.VenueDetailsViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.venue_details_fragment.*
import org.koin.android.ext.android.inject

class VenueDetailsFragment : Fragment() {

    private val viewModel: VenueDetailsViewModel by inject()

    private lateinit var venueId: String

    companion object {

        fun newInstance(venueId: VenueId) =
            VenueDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(VENUE, venueId.value)
                }
            }

        private const val VENUE = "venue"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            with(it.getString(VENUE)) {
                if (this.isNullOrEmpty()) throw RuntimeException("VenueId should not be empty")
                venueId = this
            }
        } ?: throw RuntimeException("bundle arguments should not be null")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.venue_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // since we are ready, fire the call
        viewModel.dispatchAction(DetailsAction.LoadDetails(VenueId(venueId)))

        observe(viewModel.uiState) { updateUi(it) }
    }

    private fun updateUi(state: DetailsState) {
        progress.setVisible(state.isProgressVisible)
        message.setVisible(state.message.isNotEmpty())
        message.text = state.message

        state.venue?.let {
            venue_details.setVisible(true)

            title.text = it.name
            description.text = it.description
            address.text = it.address
            contact.text = it.contactInfo
            rating.text = it.rating

            Picasso.get()
                .load(it.photoUrl?.value)
                .placeholder(R.drawable.image_placeholder)
                .fit()
                .centerCrop()
                .into(image)

        } ?: venue_details.setVisible(false)
    }

}
