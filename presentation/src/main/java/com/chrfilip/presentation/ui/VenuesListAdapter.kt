package com.chrfilip.presentation.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.chrfilip.domain.model.VenueItem
import com.chrfilip.domain.model.VenueId
import com.chrfilip.presentation.R
import kotlinx.android.synthetic.main.list_item_venue.view.*

interface ItemClickListener {
    fun onItemClick(id: VenueId)
}

class VenuesListAdapter(
    private var data: List<VenueItem>,
    private val itemClickListener: ItemClickListener
) : RecyclerView.Adapter<VenuesListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_venue, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(data[position]) {
            holder.itemView.setOnClickListener { itemClickListener.onItemClick(data[position].id) }
            holder.tvName.text = name
            holder.tvAddress.text = address
        }
    }

    override fun getItemCount(): Int = data.count()

    fun update(data: List<VenueItem>) {
        this.data = data
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvName: TextView = view.venue_name
        val tvAddress: TextView = view.venue_address
    }

}