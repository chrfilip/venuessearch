package com.chrfilip.presentation.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.chrfilip.domain.model.VenueId
import com.chrfilip.presentation.R
import com.chrfilip.presentation.common.observe
import com.chrfilip.presentation.common.setVisible
import com.chrfilip.presentation.viewmodel.*
import kotlinx.android.synthetic.main.venues_list_fragment.*
import org.koin.android.ext.android.inject

interface VenuesListCallback {
    fun showDetails(venueId: VenueId)
}

class VenuesListFragment : Fragment(), ItemClickListener {

    private lateinit var callback: VenuesListCallback

    private val viewModel: VenuesListViewModel by inject()

    private lateinit var venuesAdapter: VenuesListAdapter

    companion object {
        fun newInstance() = VenuesListFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context !is VenuesListCallback) {
            throw IllegalStateException("${this.javaClass.simpleName} can only be included in a container that implements ${VenuesListCallback::class.java.simpleName}")
        } else {
            callback = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.venues_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()

        observe(viewModel.uiState) { updateUi(it) }

        observe(viewModel.effect) { effect ->
            when (effect) {
                is SearchEffect.ItemSelected -> callback.showDetails(effect.id)
            }
        }
    }

    private fun initUi() {
        venuesAdapter = VenuesListAdapter(
            emptyList(),
            this
        )

        recyclerview.apply {
            layoutManager = LinearLayoutManager(this.context)
            hasFixedSize()
            val decorator =
                DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL)
            decorator.setDrawable(resources.getDrawable(R.drawable.divider, null))
            addItemDecoration(decorator)

            adapter = venuesAdapter
        }

        search.setOnClickListener {
            search.isIconified = false
        }

        search.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                viewModel.dispatchAction(SearchAction.SearchVenues(query))
                search.clearFocus()
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.isEmpty()) viewModel.dispatchAction(SearchAction.Clear)

                return false
            }
        })
    }

    private fun updateUi(state: SearchState) {
        progress.setVisible(state.isProgressVisible)
        message.setVisible(state.message.isNotEmpty())
        message.text = state.message

        recyclerview.setVisible(state.venues.isNotEmpty())
        venuesAdapter.update(state.venues)
    }

    override fun onItemClick(id: VenueId) {
        viewModel.dispatchAction(SearchAction.SelectItem(id))
    }

}