package com.chrfilip.presentation.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.chrfilip.domain.model.VenueId
import com.chrfilip.presentation.R

class MainActivity : AppCompatActivity(), VenuesListCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        if (savedInstanceState == null) {
            showListFragment()
        }
    }

    private fun showListFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, VenuesListFragment.newInstance())
            .commitNow()
    }

    private fun showDetailsFragment(venueId: VenueId) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, VenueDetailsFragment.newInstance(venueId))
            .addToBackStack(null)
            .commit()
    }

    override fun showDetails(venueId: VenueId) {
        showDetailsFragment(venueId)
    }
}
