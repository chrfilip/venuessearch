package com.chrfilip.presentation.model

import com.chrfilip.domain.model.ContactInfo
import com.chrfilip.domain.model.PhotoUrl
import com.chrfilip.domain.model.VenueId

class VenueUiDetails(
    val name: String,
    val address: String,
    val description: String = "--",
    val contactInfo: String = "--",
    val rating: String = "--",
    val photoUrl: PhotoUrl? = null
)