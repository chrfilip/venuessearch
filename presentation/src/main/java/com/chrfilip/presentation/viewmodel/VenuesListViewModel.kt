package com.chrfilip.presentation.viewmodel

import android.content.Context
import androidx.lifecycle.viewModelScope
import com.chrfilip.domain.common.Either
import com.chrfilip.domain.failure.Failure
import com.chrfilip.domain.model.SearchParams
import com.chrfilip.domain.model.VenueItem
import com.chrfilip.domain.model.VenueId
import com.chrfilip.domain.usecase.VenuesSearchUseCase
import com.chrfilip.presentation.R
import com.chrfilip.presentation.common.BaseViewModel
import kotlinx.coroutines.launch

sealed class SearchAction {
    data class SearchVenues(val query: String) : SearchAction()
    data class SelectItem(val id: VenueId) : SearchAction()
    object Clear : SearchAction()
}

sealed class SearchEffect {
    data class ItemSelected(val id: VenueId) : SearchEffect()
}

data class SearchState(
    val isProgressVisible: Boolean = false,
    val message: String = "",
    val venues: List<VenueItem> = emptyList()
) {
    fun showLoading() = copy(
        isProgressVisible = true,
        message = "",
        venues = emptyList()
    )

    fun showMessage(message: String) = copy(
        isProgressVisible = false,
        message = message,
        venues = emptyList()
    )

    fun showContent(venues: List<VenueItem>) = copy(
        isProgressVisible = false,
        message = "",
        venues = venues
    )
}

class VenuesListViewModel(
    private val app: Context,
    private val searchUseCase: VenuesSearchUseCase,
    initialState: SearchState = SearchState()
) : BaseViewModel<SearchState, SearchEffect>(initialState) {

    // Default search params. Modify to get different results from the api
    private val searchParams = SearchParams(
        query = "",
        intent = "checkin",
        near = "Amsterdam",
        radius = 1000,
        limit = 10
    )

    init {
        state.copy(message = app.getString(R.string.empty_message)).emit()
    }

    fun dispatchAction(action: SearchAction) {
        when (action) {
            is SearchAction.SearchVenues -> {
                state.showLoading().emit()

                viewModelScope.launch {
                    when (val result = searchUseCase.run(searchParams.copy(query = action.query))) {
                        is Either.Error -> handleFailure(result.errorVal)
                        is Either.Success -> handleSuccess(result.successVal)
                    }
                }
            }
            is SearchAction.SelectItem -> {
                SearchEffect.ItemSelected(
                    action.id
                ).effect()
            }
            SearchAction.Clear -> {
                state.showMessage(app.getString(R.string.empty_message)).emit()
            }
        }
    }

    private fun handleSuccess(venues: List<VenueItem>) {
        if (venues.isEmpty()) {
            state.showMessage(app.getString(R.string.empty_message)).emit()
        } else {
            state.showContent(venues).emit()
        }
    }

    private fun handleFailure(failure: Failure) {
        state.showMessage(failure.message).emit()
    }

}