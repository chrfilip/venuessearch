package com.chrfilip.presentation.viewmodel

import android.content.Context
import androidx.lifecycle.viewModelScope
import com.chrfilip.domain.common.Either
import com.chrfilip.domain.failure.Failure
import com.chrfilip.domain.model.ContactInfo
import com.chrfilip.domain.model.VenueId
import com.chrfilip.domain.model.VenueItem
import com.chrfilip.domain.usecase.VenueDetailsUseCase
import com.chrfilip.presentation.R
import com.chrfilip.presentation.common.BaseViewModel
import com.chrfilip.presentation.model.VenueUiDetails
import kotlinx.coroutines.launch
import java.lang.StringBuilder

sealed class DetailsAction {
    data class LoadDetails(val id: VenueId) : DetailsAction()
}

data class DetailsState(
    val isProgressVisible: Boolean = false,
    val message: String = "",
    val venue: VenueUiDetails? = null
) {
    fun showLoading() = copy(
        isProgressVisible = true,
        message = "",
        venue = null
    )

    fun showError(errorMessage: String) = copy(
        isProgressVisible = false,
        message = errorMessage,
        venue = null
    )

    fun showContent(venueUiDetails: VenueUiDetails) = copy(
        isProgressVisible = false,
        message = "",
        venue = venueUiDetails
    )
}

class VenueDetailsViewModel(
    private val app: Context,
    private val venueDetailsUseCase: VenueDetailsUseCase,
    initialState: DetailsState = DetailsState()
) : BaseViewModel<DetailsState, Nothing>(initialState) {

    private val emptyPlaceholder = "--"

    fun dispatchAction(action: DetailsAction) {
        when (action) {
            is DetailsAction.LoadDetails -> {
                state.showLoading().emit()

                viewModelScope.launch {
                    when (val result = venueDetailsUseCase.run(params = action.id)) {
                        is Either.Error -> handleFailure(result.errorVal)
                        is Either.Success -> handleSuccess(result.successVal)
                    }
                }
            }
        }
    }

    private fun handleSuccess(venue: VenueItem) {
        state.showContent(venue.toVenueUiDetails()).emit()
    }

    private fun handleFailure(failure: Failure) {
        state.showError(failure.message).emit()
    }

    private fun VenueItem.toVenueUiDetails(): VenueUiDetails {
        return VenueUiDetails(
            name = name,
            address = address,
            description = description ?: emptyPlaceholder,
            rating = if (rating == null) emptyPlaceholder else rating.toString(),
            contactInfo = contactInfo.toString(),
            photoUrl = photoUrl
        )
    }

    private fun ContactInfo?.toString(): String {
        if (this == null) return emptyPlaceholder

        return StringBuilder()
            .append(app.getString(R.string.phone))
            .append("\t: ")
            .append(this.phone ?: emptyPlaceholder)
            .append("\n")
            .append(app.getString(R.string.twitter))
            .append("\t: ")
            .append(this.twitter ?: emptyPlaceholder)
            .append("\n")
            .append(app.getString(R.string.instagram))
            .append("\t: ")
            .append(this.instagram ?: emptyPlaceholder)
            .toString()
    }

}
