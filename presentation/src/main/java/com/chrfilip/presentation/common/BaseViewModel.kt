package com.chrfilip.presentation.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel<S, E>(protected var state: S): ViewModel() {

    private val _uiState: MutableLiveData<S> = MutableLiveData()
    val uiState: LiveData<S> = startWithState(state)

    private val _effect: SingleLiveEvent<E> = SingleLiveEvent()
    val effect: LiveData<E>
        get() = _effect

    protected fun S.emit() {
        state = this
        _uiState.postValue(state)
    }

    protected fun E.effect() {
        _effect.postValue(this)
    }

    private fun startWithState(state: S): LiveData<S> {
        _uiState.postValue(state)
        return _uiState
    }

}