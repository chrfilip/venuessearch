package com.chrfilip.presentation.koin

import com.chrfilip.presentation.viewmodel.VenueDetailsViewModel
import com.chrfilip.presentation.viewmodel.VenuesListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel import org.koin.dsl.module

val presentationModule = module {

    viewModel {
        VenuesListViewModel(
            app = get(),
            searchUseCase = get()
        )
    }

    viewModel {
        VenueDetailsViewModel(
            app = get(),
            venueDetailsUseCase = get()
        )
    }
}