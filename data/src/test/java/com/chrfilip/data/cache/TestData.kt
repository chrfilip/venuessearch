package com.chrfilip.data.cache

import com.chrfilip.data.common.Venue
import com.chrfilip.data.remote.model.*
import com.chrfilip.domain.model.SearchParams
import com.nhaarman.mockitokotlin2.mock


var location: Location = mock()

val venue1 = Venue(
    id = "1",
    name = "domino's pizza",
    location = location,
    contact = null,
    rating = 6.7,
    description = "great pizza place",
    bestPhoto = null
)

val venue2 = Venue(
    id = "2",
    name = "new york pizza",
    location = location,
    contact = null,
    rating = null,
    description = null,
    bestPhoto = null
)

val venue3 = Venue(
    id = "3",
    name = "bobs's burgers",
    location = location,
    contact = null,
    rating = 7.8,
    description = null,
    bestPhoto = null
)

val venue4 = Venue(
    id = "4",
    name = "the burger venue",
    location = location,
    contact = null,
    rating = 4.1,
    description = "ultra big burgers",
    bestPhoto = null
)

val searchParams = SearchParams(
    query = "",
    intent = "checkin",
    near = "Amsterdam",
    radius = 1000,
    limit = 10
)

val venueSearchSuccessResponse = VenuesSearchResponse(
    meta = Meta(200, null, null, "2356fwe"),
    response = SearchResponse(listOf(venue1, venue2))
)

val venueDetailsSuccessResponse = VenueDetailsResponse(
    meta = Meta(200, null, null, "dfndsv64"),
    response = DetailsResponse(venue1)
)

