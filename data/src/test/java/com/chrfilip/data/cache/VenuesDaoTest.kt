package com.chrfilip.data.cache

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.chrfilip.data.common.Venue
import com.chrfilip.data.remote.model.Location
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import kotlin.test.assertEquals

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P])  // When targeting API 29 robolectric requires Java 9
class VenuesDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val database = Room.inMemoryDatabaseBuilder(
        ApplicationProvider.getApplicationContext(),
        VenuesDatabase::class.java
    ).allowMainThreadQueries().build()

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun `given venue already stored when searching with id then venue should be returned`() {
        runBlockingTest {
            // given
            database.venuesDao().insert(venue1)

            // when
            val result = database.venuesDao().venueById("1")

            // then
            assertEquals(venue1.name, result?.name)
        }
    }

    @Test
    fun `given venues already stored when searching with name then venues should be returned`() {
        runBlockingTest {
            // given
            database.venuesDao().insert(listOf(venue1, venue2, venue3, venue4))

            // when
            val result = database.venuesDao().venuesByName("pizza")

            // then
            assertEquals(2, result?.size)
        }
    }

    @Test
    fun `given venues already stored when searching with name then single venue should be returned`() {
        runBlockingTest {
            // given
            database.venuesDao().insert(listOf(venue1, venue2, venue3, venue4))

            // when
            val result = database.venuesDao().venuesByName("bob")

            // then
            assertEquals(1, result?.size)
            assertEquals("3", result?.get(0)?.id)
        }
    }

    @Test
    fun `given venue already stored when searching with id but then should return null`() {
        runBlockingTest {
            // given
            database.venuesDao().insert(venue1)

            // when
            val result = database.venuesDao().venueById("3")

            // then
            assertEquals(null, result)
        }
    }

    @Test
    fun `given venue already stored when searching with name but then should return empty list`() {
        runBlockingTest {
            // given
            database.venuesDao().insert(venue1)

            // when
            val result = database.venuesDao().venuesByName("sushi")

            // then
            assertEquals(0, result?.size)
        }
    }

}