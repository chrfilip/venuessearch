package com.chrfilip.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.chrfilip.data.cache.*
import com.chrfilip.data.common.Venue
import com.chrfilip.data.remote.base.ApiBaseRepositoryImpl
import com.chrfilip.data.remote.model.VenueDetailsResponse
import com.chrfilip.data.remote.model.VenuesSearchResponse
import com.chrfilip.data.remote.service.FoursquareApi
import com.chrfilip.data.utils.ConnectionManager
import com.chrfilip.domain.common.Either
import com.chrfilip.domain.failure.Failure
import com.chrfilip.domain.model.VenueId
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.stub
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import retrofit2.Response


class VenuesRepositoryImplTest {

    @get:Rule
    var rule = InstantTaskExecutorRule()

    private val job: Job = SupervisorJob()
    private val testCoroutineDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testCoroutineDispatcher + job)

    private val apiBaseRepository = ApiBaseRepositoryImpl()
    private val foursquareApi: FoursquareApi = mock()
    private val venuesCache: VenuesCache = mock()
    private val connectionManager: ConnectionManager = mock()

    private lateinit var sut: VenuesRepositoryImpl

    @Before
    fun setUp() {
        mockCacheInsertion()

        sut = VenuesRepositoryImpl(
            foursquareApi = foursquareApi,
            venuesCache = venuesCache,
            connectionManager = connectionManager,
            baseRepository = apiBaseRepository,
            coroutineContext = testCoroutineDispatcher
        )
    }

    @Test
    fun `given online when search api call fails then result should be failure`() {
        testScope.runBlockingTest {
            // given
            mockIsOnline()
            mockSearchApiResponse(Response.error(400, "error".toResponseBody()))

            // when
            val result = sut.searchVenues(searchParams = searchParams)

            //then
            assertEquals(true, result.errorValue() is Failure.Undefined)
        }
    }

    @Test
    fun `given online when search api call succeeds then result should be returned`() {
        testScope.runBlockingTest {
            // given
            mockIsOnline()
            mockSearchApiResponse(Response.success(venueSearchSuccessResponse))

            // when
            val result = sut.searchVenues(searchParams = searchParams)

            //then
            assertEquals(2, result.successValue()?.size)
            assertEquals(venue1.id, result.successValue()?.get(0)?.id)
            assertEquals(venue2.id, result.successValue()?.get(1)?.id)
        }
    }

    @Test
    fun `given offline when search called then cache should be used`() {
        testScope.runBlockingTest {
            // given
            mockIsOffline()
            mockCacheResultsByName(venue1, venue2)

            // when
            sut.searchVenues(searchParams = searchParams)

            //then
            verify(venuesCache, times(1)).getVenuesByName(searchParams.query)
        }
    }

    @Test
    fun `given online when details api call fails then result should be failure`() {
        testScope.runBlockingTest {
            // given
            mockIsOnline()
            mockDetailsApiResponse(Response.error(400, "error".toResponseBody()))

            // when
            val venueId = VenueId("1")
            val result = sut.venueDetails(venueId)

            //then
            assertEquals(true, result.errorValue() is Failure.Undefined)
        }
    }

    @Test
    fun `given online when details api call succeeds then result should be returned`() {
        testScope.runBlockingTest {
            // given
            mockIsOnline()
            mockDetailsApiResponse(Response.success(venueDetailsSuccessResponse))

            // when
            val venueId = VenueId("1")
            val result = sut.venueDetails(venueId)

            //then
            assertEquals(venue1.id, result.successValue()?.id)
        }
    }

    @Test
    fun `given offline when details called then cache should be used`() {
        testScope.runBlockingTest {
            // given
            mockIsOffline()
            mockCacheResultById(venue1)

            // when
            val venueId = VenueId("1")
            sut.venueDetails(venueId)

            //then
            verify(venuesCache, times(1)).getVenueById(venueId.value)
        }
    }

    private fun mockSearchApiResponse(response: Response<VenuesSearchResponse>) {
        foursquareApi.stub {
            onBlocking {
                foursquareApi.venuesSearch(
                    anyNonNull(),
                    anyNonNull(),
                    anyNonNull(),
                    anyNonNull(),
                    anyNonNull()
                )
            }.thenReturn(response)
        }
    }


    private fun mockDetailsApiResponse(response: Response<VenueDetailsResponse>) {
        foursquareApi.stub {
            onBlocking { foursquareApi.venueDetails(anyNonNull()) }
                .thenReturn(response)
        }
    }

    private fun mockIsOnline() {
        `when`(connectionManager.isOnline()).thenReturn(true)
    }

    private fun mockIsOffline() {
        `when`(connectionManager.isOnline()).thenReturn(false)
    }

    private fun mockCacheResultsByName(vararg venues: Venue) {
        venuesCache.stub {
            onBlocking { getVenuesByName(anyString()) }
                .thenReturn(Either.Success(venues.asList()))
        }
    }

    private fun mockCacheResultById(venue: Venue) {
        venuesCache.stub {
            onBlocking { getVenueById(anyString()) }
                .thenReturn(Either.Success(venue))
        }
    }

    private fun mockCacheInsertion() {
        venuesCache.stub {
            onBlocking { insert(any<Venue>()) }
                .thenReturn(Either.Success(Unit))
        }

        venuesCache.stub {
            onBlocking { insert(any<List<Venue>>()) }
                .thenReturn(Either.Success(Unit))
        }
    }

    private inline fun <reified T> anyNonNull(): T = Mockito.any<T>(T::class.java)
}