package com.chrfilip.data.cache

import androidx.room.TypeConverter
import com.chrfilip.data.remote.model.BestPhoto
import com.chrfilip.data.remote.model.Contact
import com.chrfilip.data.remote.model.Location
import com.google.gson.Gson

/**
 * Room does not "know" how to save data classes so
 * we provide the converter so it will store as json string
 */
class Converters {

    private val gson = Gson()

    @TypeConverter
    fun fromLocationJson(value: String?): Location? {
        return value?.let { gson.fromJson(it, Location::class.java) }
    }

    @TypeConverter
    fun locationToJson(location: Location?): String? {
        return location?.let { gson.toJson(it) }
    }

    @TypeConverter
    fun fromContactJson(value: String?): Contact? {
        return value?.let { gson.fromJson(it, Contact::class.java) }
    }

    @TypeConverter
    fun contactToJson(contact: Contact?): String? {
        return contact?.let { gson.toJson(it) }
    }

    @TypeConverter
    fun fromBestPhotoJson(value: String?): BestPhoto? {
        return value?.let { gson.fromJson(it, BestPhoto::class.java) }
    }

    @TypeConverter
    fun bestPhotoToJson(bestPhoto: BestPhoto?): String? {
        return bestPhoto?.let { gson.toJson(it) }
    }

}