package com.chrfilip.data.cache

import com.chrfilip.data.common.Venue
import com.chrfilip.domain.common.Either
import com.chrfilip.domain.failure.Failure


class VenuesCacheImpl(private val venuesDao: VenuesDao):
    VenuesCache {

    override suspend fun insert(venue: Venue): Either<Failure, Unit> {
        venuesDao.insert(venue)
        return Either.Success(Unit) // placeholder for future refactoring
    }

    override suspend fun insert(venues: List<Venue>): Either<Failure, Unit> {
        venuesDao.insert(venues)
        return Either.Success(Unit) // placeholder for future refactoring
    }

    override suspend fun getVenuesByName(name: String): Either<Failure, List<Venue>> {
        return venuesDao.venuesByName(name)?.let {
            Either.Success(it)
        } ?: Either.Error(Failure.CacheFailure("Could not find venues"))
    }

    override suspend fun getVenueById(id: String): Either<Failure, Venue> {
        return venuesDao.venueById(id)?.let {
            Either.Success(it)
        } ?: Either.Error(Failure.CacheFailure("Could not find venue"))
    }
}