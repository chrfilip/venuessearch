package com.chrfilip.data.cache

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.chrfilip.data.common.Venue

@Dao
interface VenuesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(venues: List<Venue>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(venue: Venue)

    // in order to simulate api's query, database searches for the query string inside name
    @Query("SELECT * FROM venues WHERE name LIKE '%' || :name  || '%' LIMIT 10")
    suspend fun venuesByName(name: String): List<Venue>?

    @Query("SELECT * FROM venues WHERE id = :id LIMIT 1 ")
    suspend fun venueById(id: String): Venue?
}