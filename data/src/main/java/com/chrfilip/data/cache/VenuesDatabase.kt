package com.chrfilip.data.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.chrfilip.data.common.Venue

@Database(
        entities = [Venue::class],
        version = 1,
        exportSchema = false
)
@TypeConverters(Converters::class)
abstract class VenuesDatabase : RoomDatabase() {

    abstract fun venuesDao(): VenuesDao
}