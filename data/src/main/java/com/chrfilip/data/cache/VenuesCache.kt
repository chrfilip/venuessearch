package com.chrfilip.data.cache

import com.chrfilip.data.common.Venue
import com.chrfilip.domain.common.Either
import com.chrfilip.domain.failure.Failure


interface VenuesCache {

    suspend fun insert(venue: Venue): Either<Failure, Unit>
    suspend fun insert(venues: List<Venue>): Either<Failure, Unit>
    suspend fun getVenuesByName(name: String): Either<Failure, List<Venue>>
    suspend fun getVenueById(id: String): Either<Failure, Venue>
}