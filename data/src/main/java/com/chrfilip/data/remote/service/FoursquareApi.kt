package com.chrfilip.data.remote.service

import com.chrfilip.data.remote.model.VenueDetailsResponse
import com.chrfilip.data.remote.model.VenuesSearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FoursquareApi {

    @GET("venues/search")
    suspend fun venuesSearch(
        @Query("intent") intent: String,
        @Query("query") query: String,
        @Query("near") near: String,
        @Query("radius") radius: Int,
        @Query("limit") limit: Int
    ): Response<VenuesSearchResponse>

    @GET("venues/{venue_id}")
    suspend fun venueDetails(@Path("venue_id") venueId: String): Response<VenueDetailsResponse>

}