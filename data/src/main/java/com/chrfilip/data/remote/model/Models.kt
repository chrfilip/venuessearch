package com.chrfilip.data.remote.model

interface ApiResponse<T> {
    val meta: Meta
    val response: T
}

data class VenuesSearchResponse(
    override val meta: Meta,
    override val response: SearchResponse
) : ApiResponse<SearchResponse>

data class VenueDetailsResponse(
    override val meta: Meta,
    override val response: DetailsResponse
) : ApiResponse<DetailsResponse>




