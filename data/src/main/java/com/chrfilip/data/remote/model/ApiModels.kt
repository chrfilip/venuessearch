package com.chrfilip.data.remote.model

import com.chrfilip.data.common.Venue

data class Meta (
    val code : Int,
    val errorType: String?,
    val errorDetail: String?,
    val requestId : String
)

data class SearchResponse (
    val venues : List<Venue>
)

data class DetailsResponse (
    val venue: Venue
)

data class Contact (
    val phone : String?,
    val formattedPhone : String?,
    val twitter : String?,
    val instagram : String?
)

data class LabeledLatLngs (
    val label : String,
    val lat : Double,
    val lng : Double
)

data class Location (
    val address : String?,
    val lat : Double,
    val lng : Double,
    val labeledLatLngs : List<LabeledLatLngs>,
    val postalCode : String,
    val cc : String,
    val city : String,
    val state : String,
    val country : String,
    val formattedAddress : List<String>
)

data class BestPhoto (
    val id : String,
    val createdAt : Int,
    val source : Source,
    val prefix : String,
    val suffix : String,
    val width : Int,
    val height : Int,
    val visibility : String
)

data class Source (
    val name : String,
    val url : String
)





