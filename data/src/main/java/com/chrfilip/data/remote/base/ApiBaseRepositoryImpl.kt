package com.chrfilip.data.remote.base

import com.chrfilip.data.remote.model.ApiResponse
import com.chrfilip.domain.common.Either
import com.chrfilip.domain.failure.Failure
import retrofit2.Response

/**
 * A class providing centralized api response handling
 */
class ApiBaseRepositoryImpl : ApiBaseRepository {

    override suspend fun <R : Any, T : ApiResponse<R>> apiCall(call: suspend () -> Response<T>): Either<Failure, R> {
        val response: Response<T>
        try {
            response = call.invoke()
        } catch (t: Throwable) {
            return Either.Error(Failure.NetworkFailure(t.message ?: "network failure"))
        }

        response.body()?.let { body ->
            return if (response.isSuccessful) {
                Either.Success(body.response)
            } else {
                Either.Error(
                    Failure.ApiFailure(
                        errorMessage = body.meta.errorDetail ?: "api failure",
                        errorCode = body.meta.code
                    )
                )
            }
        } ?: return Either.Error(Failure.Undefined)
    }

}