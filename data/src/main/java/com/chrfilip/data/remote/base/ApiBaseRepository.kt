package com.chrfilip.data.remote.base

import com.chrfilip.data.remote.model.ApiResponse
import com.chrfilip.domain.common.Either
import com.chrfilip.domain.failure.Failure
import retrofit2.Response

interface ApiBaseRepository {
    suspend fun <R : Any, T : ApiResponse<R>> apiCall(call: suspend () -> Response<T>): Either<Failure, R>
}