package com.chrfilip.data.common

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.chrfilip.data.remote.model.BestPhoto
import com.chrfilip.data.remote.model.Contact
import com.chrfilip.data.remote.model.Location

@Entity(tableName = "venues")
data class Venue (
    @PrimaryKey val id : String,
    val name : String,
    val location : Location,
    val contact: Contact?,
    val rating : Double?,
    val description : String?,
    val bestPhoto: BestPhoto?
)