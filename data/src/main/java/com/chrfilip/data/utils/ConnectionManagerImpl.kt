package com.chrfilip.data.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.TYPE_MOBILE
import android.net.ConnectivityManager.TYPE_WIFI
import android.net.NetworkCapabilities.TRANSPORT_CELLULAR
import android.net.NetworkCapabilities.TRANSPORT_WIFI
import android.os.Build


interface ConnectionManager {
    fun isOnline(): Boolean
}

/**
 * A class for inquiring network state
 */
class ConnectionManagerImpl(private val app: Context) : ConnectionManager {

    @Suppress("DEPRECATION")
    override fun isOnline(): Boolean {

        val cm = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return if (Build.VERSION.SDK_INT < 23) {
            cm.activeNetworkInfo?.let {
                (it.isConnected && (it.type == TYPE_WIFI || it.type == TYPE_MOBILE))
            } ?: false
        } else {
            cm.activeNetwork?.let {
                val capabilities = cm.getNetworkCapabilities(it)
                capabilities?.let {
                    capabilities.hasTransport(TRANSPORT_CELLULAR) || capabilities.hasTransport(TRANSPORT_WIFI)
                } ?: false
            } ?: false
        }

    }

}