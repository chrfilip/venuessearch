package com.chrfilip.data.utils

import com.chrfilip.data.common.Venue
import com.chrfilip.data.remote.model.BestPhoto
import com.chrfilip.data.remote.model.Contact
import com.chrfilip.data.remote.model.Location
import com.chrfilip.domain.model.ContactInfo
import com.chrfilip.domain.model.PhotoUrl
import com.chrfilip.domain.model.VenueId
import com.chrfilip.domain.model.VenueItem

fun List<Venue>.toVenueItems(): List<VenueItem> {
    val venues = mutableListOf<VenueItem>()
    this.forEach {
        venues.add(
            VenueItem(
                VenueId(it.id),
                it.name,
                it.location.toAddress()
            )
        )
    }
    return venues
}

fun Venue.toVenueItemWithDetails(): VenueItem {
    return VenueItem(
        id = VenueId(this.id),
        name = this.name,
        address = this.location.toAddress(),
        description = this.description,
        photoUrl = this.bestPhoto.toPhotoUrl(),
        contactInfo = this.contact.toContactInfo(),
        rating = this.rating
    )
}

fun Location.toAddress(): String {
    return this.formattedAddress.joinToString(separator = ", ")
}

fun BestPhoto?.toPhotoUrl(): PhotoUrl? {
    if (this == null) return null

    // construct photo url according to the documentation
    // https://developer.foursquare.com/docs/api-reference/photos/details/
    return PhotoUrl("${this.prefix}$API_IMAGE_DIMENSIONS${this.suffix}")
}

private fun Contact?.toContactInfo(): ContactInfo? {
    if (this == null) return null

    return ContactInfo(
        phone = this.formattedPhone,
        instagram = this.instagram,
        twitter = this.twitter
    )
}