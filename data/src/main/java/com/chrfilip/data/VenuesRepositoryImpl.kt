package com.chrfilip.data

import com.chrfilip.data.cache.VenuesCache
import com.chrfilip.data.remote.base.ApiBaseRepository
import com.chrfilip.data.remote.service.FoursquareApi
import com.chrfilip.data.utils.ConnectionManager
import com.chrfilip.data.utils.toVenueItemWithDetails
import com.chrfilip.data.utils.toVenueItems
import com.chrfilip.domain.common.Either
import com.chrfilip.domain.failure.Failure
import com.chrfilip.domain.model.SearchParams
import com.chrfilip.domain.model.VenueItem
import com.chrfilip.domain.model.VenueId
import com.chrfilip.domain.repository.VenuesRepository
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class VenuesRepositoryImpl(
    private val foursquareApi: FoursquareApi,
    private val venuesCache: VenuesCache,
    private val connectionManager: ConnectionManager,
    private val baseRepository: ApiBaseRepository,
    private val coroutineContext: CoroutineContext
) : VenuesRepository {

    override suspend fun searchVenues(searchParams: SearchParams): Either<Failure, List<VenueItem>> {

        return withContext(coroutineContext) {
             if (connectionManager.isOnline()) {
                val result = baseRepository.apiCall {
                    foursquareApi.venuesSearch(
                        intent = searchParams.intent,
                        query = searchParams.query,
                        near = searchParams.near,
                        radius = searchParams.radius,
                        limit = searchParams.limit
                    )
                }

                 // transform result to domain models
                when (result) {
                    is Either.Error -> result
                    is Either.Success -> {
                        val venues = result.successVal.venues
                        // insert values in cache
                        venuesCache.insert(venues)
                        Either.Success(venues.toVenueItems())
                    }
                }
            } else {
                 // transform result to domain models
                 when (val result = venuesCache.getVenuesByName(searchParams.query)) {
                     is Either.Error -> result
                     is Either.Success -> {
                         Either.Success(result.successVal.toVenueItems())
                     }
                 }
            }
        }
    }

    override suspend fun venueDetails(venueId: VenueId): Either<Failure, VenueItem> {
        return withContext(coroutineContext) {
            if (connectionManager.isOnline()) {
                val result =  baseRepository.apiCall {
                    foursquareApi.venueDetails(venueId = venueId.value)
                }

                // transform result to domain models
                when (result) {
                    is Either.Error -> result
                    is Either.Success -> {
                        val venue = result.successVal.venue
                        // insert value in cache
                        venuesCache.insert(venue)
                        Either.Success(venue.toVenueItemWithDetails())
                    }
                }
            } else {
                // transform result to domain models
                when (val result = venuesCache.getVenueById(venueId.value)) {
                    is Either.Error -> result
                    is Either.Success -> {
                        Either.Success(result.successVal.toVenueItemWithDetails())
                    }
                }
            }
        }
    }
}