package com.chrfilip.data.koin

import androidx.room.Room
import com.chrfilip.data.*
import com.chrfilip.data.cache.VenuesCache
import com.chrfilip.data.cache.VenuesCacheImpl
import com.chrfilip.data.cache.VenuesDatabase
import com.chrfilip.data.remote.base.ApiBaseRepository
import com.chrfilip.data.remote.base.ApiBaseRepositoryImpl
import com.chrfilip.data.remote.service.FoursquareApi
import com.chrfilip.data.utils.*
import com.chrfilip.domain.repository.VenuesRepository
import kotlinx.coroutines.Dispatchers
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

val dataModule = module {
    single(named("IO")) { Dispatchers.IO }

    single<ConnectionManager> { ConnectionManagerImpl(get()) }

    single<VenuesRepository> {
        VenuesRepositoryImpl(
            get(),
            get(),
            get(),
            get(),
            get(named("IO"))
        )
    }

    single {
        Room.databaseBuilder(get(), VenuesDatabase::class.java, "venues-db")
            .build()
    }

    single { get<VenuesDatabase>().venuesDao() }

    single<VenuesCache> { VenuesCacheImpl(get()) }

    single<ApiBaseRepository> { ApiBaseRepositoryImpl() }

    single {
        interceptor()
    }

    single {
        okHttpClient(get())
    }

    single {
        retrofit(get())
    }

    single {
        api(get())
    }
}

private fun retrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(API_BASE_URL)
        .client(okHttpClient)
        .build()
}

private fun api(retrofit: Retrofit): FoursquareApi {
    return retrofit.create(FoursquareApi::class.java)
}

private fun okHttpClient(interceptors: List<Interceptor>): OkHttpClient {
    val clientBuilder = OkHttpClient.Builder()
        .followRedirects(false)
        .readTimeout(API_TIMEOUT, TimeUnit.SECONDS)
    interceptors.forEach {
        clientBuilder.addInterceptor(it)
    }
    return clientBuilder.build()
}


private fun interceptor(): List<Interceptor> {
    val interceptors = mutableListOf<Interceptor>()

    val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    interceptors.add(loggingInterceptor)

    val queryInterceptor = object : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            var request = chain.request()
            val urlBuilder = request.url.newBuilder()

            urlBuilder.addQueryParameter("v", getApiVersion()).build()
            urlBuilder.addQueryParameter("client_id",
                API_CLIENT_ID
            ).build()
            urlBuilder.addQueryParameter("client_secret",
                API_CLIENT_SECRET
            ).build()

            request = request.newBuilder().url(urlBuilder.build()).build()

            return chain.proceed(request)
        }
    }

    interceptors.add(queryInterceptor)

    return interceptors
}

private fun getApiVersion(): String {
    val formatter = SimpleDateFormat(API_VERSION_FORMAT, Locale.getDefault())
    val date = Calendar.getInstance().time
    return formatter.format(date)
}
